# Ex vivo imaging reveals the spatiotemporal control of ovulation


## Raw data 
Raw data files can be obtained from Gene Expression Omnibus under accesion [GSE255274](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE255274).

## Contents

Scripts to reproduce the analyses, figures, and source files related to the single-cell RNA sequencing data in the manuscript.

## Support

If there are questions regarding the scripts, please email Sarah Penir (spenir@mpinat.mpg.de).

## Citation 

Thomas, C., Marx, T.L., Penir, S.M., Schuh M. Ex vivo imaging reveals the spatiotemporal control of ovulation. Nat Cell Biol (2024). https://doi.org/10.1038/s41556-024-01524-6